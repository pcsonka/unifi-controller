#!/bin/bash
set -e

mongosh <<EOF
use admin
db.getSiblingDB('$MONGO_DB').createUser({
    user: '$MONGO_USER',
    pwd: '$MONGO_PASSWORD',
    roles: [{
      role: "dbOwner",
      db: '$MONGO_DB'
    }]
});
db.getSiblingDB("unifi_stat").createUser({
    user: '$MONGO_USER',
    pwd: '$MONGO_PASSWORD',
    roles: [{
      role: "dbOwner",
      db: "unifi_stat"
    }]
});
EOF