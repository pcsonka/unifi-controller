# Unifi Controller sample setup

Docker based setup built on
- [LinuxServer.io Unifi-network-application](https://github.com/linuxserver/docker-unifi-network-application)
- with [LinuxServer.io SWAG](https://github.com/linuxserver/docker-swag) for easy LetsEncrypt SSL
- and [Mongo](https://hub.docker.com/_/mongo) database.

## Usage

Just clone the repo, make a new .env based on .env.example and `docker compose up`. If everything works the way it should the controller will be available on your https://{URL}. Nginx is set up to proxy to the unifi controller's 8443 port.